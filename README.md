# Curso GIT

Proyecto creado para realizar las tareas indicadas en el curso GIT


--------- CURSO GIT ------------
EMAIL: tutoria@grupoid.es
TUTOR: Antonio Gago
--------------------------------

Comparador de ficheros: kdiff3
--------------------------------

Para configurar los datos que se van a mostrar en los commit se usarán los siguientes comandos: 
	- git config --global user.name "Javier Baena"
	- git config --global user.email fjbaena@isotrol.com


Inicializar repositorio local (empezar repositorio desde 0)
	- git init
Queda inicializado el repositorio local.

--------------------------------

Clonar un repositorio ya existente
	- git clone "url" carpeta destinio
En caso de no indicar carpeta destino se creara en la carpeta actual dentro de la carpeta del repositorio
	- git clone  https://github.com/libgit2/libgit2
Lo creará en el directorio actual en la carpeta libgit2

---------------
---- STAGE ----
---------------
Se pueden utilizar patrones para subir o hacer acciones sobre un conjunto de ficheros
	- git add *.html 
	- git add "folderName/*"

------------------
---- COMPARAR ----
------------------
Comparar los ficheros que están como modificados pero que no han sido añadidos al area de preparación (Staged)
	- git diff
Se mostrará la parte del fichero que ha sido modificada. 


Para comparar los ficheros que estén en el area de preparación (Stage) habrá que añadir --staged
	- git diff --staged 

-------------------------------
---- commit sin comentario ----
-------------------------------
Si intentamos realizar un commit sin añadir un mensaje, git abrirá el editor de texto que se configuró en la instalación o configuración
y nos pedirá que añadamos un comentario.
	- git commit


--------------------------------
---- commit saltando staged ----
--------------------------------
Para realizar un commit saltandonos el area de preparación (staged), tan solo será necesario ejecutar el siguiente comando: 
	- git commit -a -m "Commit saltando el paso del area de preparación (staged)"

------------------------
---- Borrar archivo ----
------------------------
Para borrar un archivo, lo mejor no es borrar directamente el archivo desde el sistema de ficheros. 
Lo más recomendado es utilizar los comandos de git para que quede registrado correctamente el borrado del mismo. 
	- git rm nombreFichero

----------------------------
--- Borrado del registro ---
----------------------------

	- git rm --cached nombreFichero

--------------------------
---- Ignorar Ficheros ----
--------------------------
Cuando queramos que algunos ficheros tipo .project, .... abrá que crear el fichero .gitignore . En dicho fichero habrá que introducir 
los ficheros que no queremos realizar seguimiento por git.

-------------------------
---- git log gráfico ----
-------------------------
	- gitk

------------------------
---- Deshacer Cosas ----
------------------------
Para realizar un Deshacer: 
	- git commit --amend
Utiliza el stage para la confirmación. Si no has hecho ningún cambio desde la última confirmación (Commit) todo será igual y podrás cambiar el mensaje de confirmación.
Al poner el --amend, estás realizando un nuevo commit sobre el anterior, quedando como si se hubiese realizado solo un commit.


***********************************
***********26/02/2020**************
***********************************

-----------------------------------
----Añadir repositorios remotos----
-----------------------------------
	- git remote add "nombreRepositorio" "urlRepositorio"
 	
------------------------------------
---- Descargar rama sin mezclar ----
------------------------------------
	- git fetch nombreRepositorio

--------------------------------
---- Crear etiqueta marcada ----
--------------------------------
	- git tag -a versión -m "Comentario"

---------------------------
---- Etiquetado tardío ----
---------------------------
	- git tag -a versión "checksum del momento deseado, con los 4 primero es suficiente" -m "Comentario"

Para que la etiqueta se vea reflejada en el repositorio remoto, hay que hacerle push
	- git push --tags

---------------------------
---- Creación de alías ----
---------------------------
Creamos alias para el comando de status
	- git config --global alias.st status

Creamos alias para el comando de commit y otros comandos
	- git config --global alias.ci commit
	- git config --global alias.co checkout 
	- git config --global alias.br branch

Se pueden crear alias para un conjunto de comandos
	- git config --global alias.last 'log -1 HEAD'



-------------------------
----- Crear de Rama -----
-------------------------
	- git branch nombreRama

-----------------------------------
----- Crear y cambiar de Rama -----
-----------------------------------
	- git checkout -b nombreRama

---------------------------
----- Estado de ramas -----
---------------------------
	- git log --pretty=oneline --decorate --graph --all 

-------------------------
---- Cambiar de Rama ----
-------------------------
	- git checkout nombreRama

-------------------------------------------
---- Fusionar de rama hotfix en master ----
-------------------------------------------
	- git checkout master
	- git merge hotfix

Si existen conflictos en el merge, nos indicará que ficheros son los que tienen conflictos. Se resuelven los conflictos.
Para que los conflictos arreglados se guarden hay que hacer el commit de los ficheros resueltos.

-------------------------
----- Eliminar rama -----
-------------------------
	- git branch -d nombreRama
Si al eliminar una rama que no se encuentra mergeada, nos dará un aviso informando que la rama que vamos a borrar no está mezclada.

-----------------------------
---- Ver ramas no merged ----
-----------------------------
	- git branch --no-merged

--------------------------
---- Ver ramas merged ----
--------------------------
	- git branch --merged

------------------------
---- abortar merged ----
------------------------
	- git merge --abort

---------------------------------
---- Rebase / reorganización ----
---------------------------------
La diferencia con el merge es que no realiza un nuevo commit, si no que lo hace sobre el último commit.

-----------------------
---- Borrar commit ----
-----------------------
	- git reset --har "CheckSum"

Al realizar este comando, nos va a mover el head al checksum indicado, y todos los commit que estuviesen entre el head y el checksum indicado
se perderán, incluyendo ficheros añadidos entre medio. No hay marcha atrás. 

Una forma de poder hacerlo con la opción de poder volver atrás es con el comando soft.
	- git reset --soft "CheckSum"

De este modo, haces un reset pero sin hacer un commit. Es decir. --har es un soft sin hacer commit. Al hacer un --soft y realizamos commit, 
es lo mismo que hacer el --har

-------------------------------------
---- Guardado rápido provisional ----
-------------------------------------
	- git stash
	- git stash list
	- git stash apply //Si tuviese más de uno debería espeficiarlo stash@{1}, si quiero el stash que se encuentra en al posición 1 de la pila.

--------------------------
---- Fork Bifurcación ----
--------------------------
Es una clonación de un repositorio creando otro completamente independiente al anterior. Es util, para poder hacer contribuciones seguras. Al ser una copia independiente del 
repositorio puede realizar acciones sin afectar al repositorio origen. Una vez terminada la contribución se puede incorporar al original por el propietario del repositorio. 
Al ser un repositorio independiente, no es necesario darle permisos al repositorio original.


------------------
---- git flow ----
------------------
Utilidad para utilizar git con ramas especificas definidas según el uso que se vaya a utilizar. 
Incluye comandos en los cuales se hace de modo automático la creación, mezcla y eliminación de ramas. 
